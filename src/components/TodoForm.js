import React, { useState } from "react";
import useInput from "../hooks/useInput";
import "./component.css";

function TodoForm(props) {
  const [task, setTask, resetTask] = useInput("");
  const [priority, setPriority, resetPriority] = useInput("");
  const [taskError, setTaskError] = useState("");
  const [priorityError, setPriorityError] = useState("");

  function validate(name, value) {
    if (!value) {
      return `${name} is required.`;
    } else {
      return "";
    }
  }

  function handleSubmit(event) {
    event.preventDefault();

    let validationErrors = {};

    validationErrors["task"] = validate("task", task);
    validationErrors["priority"] = validate("priority", priority);

    if (validationErrors.task !== "" || validationErrors.priority !== "") {
      setTaskError(validationErrors.task);
      setPriorityError(validationErrors.priority);
      return;
    }

    props.handleTodoForm({ task, priority, completed: false, id: Date.now() });
    resetTask();
    resetPriority();
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div className="form-control">
          <label>Todo</label>
          <input
            className="input"
            placeholder="Add Task"
            value={task}
            {...setTask}
          />
          <span className="error">{taskError}</span>
        </div>
        <div className="form-control">
          <label>Priority</label>
          <select className="select" value={priority} {...setPriority}>
            <option>Select Priority</option>
            <option value="low">Low</option>
            <option value="medium">Medium</option>
            <option value="high">High</option>
          </select>
          <span className="error">{priorityError}</span>
        </div>
        <button className="add-button" type="submit">
          Add
        </button>
      </form>
    </div>
  );
}

export default TodoForm;
