import React from "react";
import "./component.css";
import { BsCheck2, BsFillArchiveFill } from "react-icons/bs";

function TodoList(props) {
  let todo_list;
  let { list, markAsDone, removeTask } = props;
  todo_list = list?.length
    ? list?.map((list, index) => {
        if (list.completed) {
          return (
            <li className="todo_list" key={list.id}>
              <dl className="todo-details completed">
                <dt className="todo-name">Task - {list.task}</dt>
                <dd className="todo-priority">Priority - {list.priority}</dd>
              </dl>
              <div className="todo_action">
                <BsCheck2 className="mark-as-done" disabled={true} />
                <BsFillArchiveFill
                  className="icon"
                  type="button"
                  title="Delete"
                  onClick={() => removeTask(list?.id)}
                />
              </div>
            </li>
          );
        } else {
          return (
            <li className="todo_list" key={list?.id}>
              <dl className="todo-details">
                <dt className="todo-name">Task - {list?.task}</dt>
                <dd className="todo-priority">Priority - {list?.priority}</dd>
              </dl>

              <div className="todo_action">
                <BsCheck2
                  className="icon"
                  value={index}
                  title="Mark as Closed"
                  onClick={() => markAsDone(list.id)}
                />

                <BsFillArchiveFill
                  className="icon"
                  type="button"
                  title="Delete"
                  onClick={() => removeTask(list.id)}
                />
              </div>
            </li>
          );
        }
      })
    : null;

  return (
    <div className="block todoList">
      <ul>{todo_list}</ul>
    </div>
  );
}

export default TodoList;
